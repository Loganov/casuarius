pluginManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
    }
}

rootProject.name = "Casuarius"
include(":casuarius-core-module")
include(":android-sample")
include(":casuarius-android-storage")