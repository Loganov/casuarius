package com.mintrocket.casuarius.utils

import kotlinx.serialization.json.Json

/**
 * JsonSerializer используется для сериализации объектов
 */
internal object JsonSerializer  {
    operator fun invoke() = Json {
        encodeDefaults = true
    }
}