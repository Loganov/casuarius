object Version {

    // Kotlin
    internal const val KOTLIN_VERSION = "1.6.10"
    internal const val SERIALIZATION_VERSION = "1.3.2"

    // Android
    internal const val ANDR_SECURITY_VERSION = "1.0.0"
    internal const val ANDR_TEST_VERSION = "1.4.0"
    internal const val ANDR_CORE_KTX_VERSION = "1.7.0"

    object Tools {
        const val compileSdkVersion = 32
        const val minSdkVersion = 23 // крипта штука работает только на 23+. Если есть варик что-то ниже заюзать - велком
        const val targetSdkVersion = 32
    }
}

object Libs {

    object Kotlin {
        const val nativeUtils = "org.jetbrains.kotlin:kotlin-native-utils:${Version.KOTLIN_VERSION}"
        const val serialization = "org.jetbrains.kotlinx:kotlinx-serialization-json:${Version.SERIALIZATION_VERSION}"
    }

    object Android {
        object Security {
            const val security = "androidx.security:security-crypto:${Version.ANDR_SECURITY_VERSION}"
            const val coreKtx = "androidx.core:core-ktx:${Version.ANDR_CORE_KTX_VERSION}"
        }

        object Test {
            const val runner = "androidx.test:runner:${Version.ANDR_TEST_VERSION}"
            const val rules = "androidx.test:rules:${Version.ANDR_TEST_VERSION}"
            const val core = "androidx.test:core:${Version.ANDR_TEST_VERSION}"
        }

    }
}