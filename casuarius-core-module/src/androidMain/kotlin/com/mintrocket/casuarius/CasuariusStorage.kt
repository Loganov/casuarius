package com.mintrocket.casuarius

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import kotlinx.serialization.KSerializer

/**
 * Android имплементация CasuariusStorage
 *
 * @param applicationContext контекст для записи на диск
 * @param storageFileName название файла
 * @param masterKeyAlias мастер ключ
 * @param commit false - отложенная запись, true - немедленная запись
 *
 */

actual open class CasuariusStorage(
    private val applicationContext: Context,
    private val storageFileName: String,
    private val masterKeyAlias: String = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC),
    private val commit : Boolean = false
) {

    private val prefs: SharedPreferences by lazy {
        EncryptedSharedPreferences.create(
            storageFileName,
            masterKeyAlias,
            applicationContext,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )
    }

    actual fun remove(key: String) {
        prefs.edit(commit) {
            remove(key)
        }
    }

    actual fun hasValue(key: String): Boolean {
       return prefs.contains(key)
    }

    actual fun clear() {
        prefs.edit(commit) {
            clear()
        }
    }

    actual fun put(key: String, value: String) {
        prefs.edit(commit) {
            putString(key, value)
        }
    }

    actual fun string(key: String, defaultValue: String?): String? {
        return prefs.getString(key, defaultValue)
    }

    actual fun put(key: String, value: Int) {
        prefs.edit(commit) {
            putInt(key, value)
        }
    }

    actual fun int(key: String, defaultValue: Int?): Int? {
        return prefs.getInt(key, defaultValue ?: 0)
    }

    actual fun put(key: String, value: Float) {
        prefs.edit(commit) {
            putFloat(key, value)
        }
    }

    actual fun float(key: String, defaultValue: Float?): Float? {
        return prefs.getFloat(key, defaultValue ?: 0f)
    }

    actual fun put(key: String, value: Long) {
        prefs.edit(commit) {
            putLong(key, value)
        }
    }

    actual fun long(key: String, defaultValue: Long?): Long? {
        return prefs.getLong(key, defaultValue ?: 0)
    }

    actual fun put(key: String, value: Double) {
        prefs.edit(commit) {
            putLong(key, value.toRawBits())
        }
    }

    actual fun double(key: String, defaultValue: Double?): Double? {
        val rawValue = prefs.getLong(key, defaultValue?.toRawBits() ?: 0)
        return Double.fromBits(rawValue)
    }

    actual fun put(key: String, value: Boolean) {
        prefs.edit(commit) {
            putBoolean(key, value)
        }
    }

    actual fun boolean(key: String, defaultValue: Boolean?): Boolean? {
        return prefs.getBoolean(key, defaultValue ?: false)
    }

    actual fun <T> put(key: String, value: T, kSerializer: KSerializer<T>) {
        prefs.edit(commit) {

        }
    }

    actual  fun <T> serializable(key: String, kSerializer: KSerializer<T>): T? {
        return null
    }
}