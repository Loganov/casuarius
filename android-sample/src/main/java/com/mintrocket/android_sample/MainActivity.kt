package com.mintrocket.android_sample

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.*
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.mintrocket.android_sample.ui.theme.CasuariusTheme
import com.mintrocket.casuarius.CasuariusStorage

@ExperimentalMaterial3Api
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        
        val storage = CasuariusStorage(applicationContext, "test_storage")

        setContent {
            CasuariusTheme {
                Scaffold(Modifier.fillMaxSize()) {
                    Column(
                        modifier = Modifier
                            .fillMaxSize()
                            .verticalScroll(rememberScrollState()),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Spacer(modifier = Modifier.height(100.dp))
                        Text(text = "Test Screen")
                        Spacer(modifier = Modifier.height(16.dp))
                        Button(onClick = { 
                            storage.put("test", "test data")
                        }) {
                            Text(text = "Save test string")

                        }

                        Button(onClick = {

                        }) {
                            Text(text = "Save test int")

                        }
                        
                        var data by remember { mutableStateOf("") }
                        Spacer(modifier = Modifier.height(16.dp))
                        Button(onClick = {
                            data = storage.string("test") ?: "Data empty"
                        }) {
                            Text(text = "Get save data")

                        }

                        Button(onClick = {
                            data = storage.string("test") ?: "Data empty"
                        }) {
                            Text(text = "Get save")

                        }

                        Spacer(modifier = Modifier.height(16.dp))
                        Text(text = data)

                    }
                }
            }
        }
    }
}