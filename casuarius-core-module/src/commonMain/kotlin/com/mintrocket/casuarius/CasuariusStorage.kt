package com.mintrocket.casuarius

import kotlinx.serialization.KSerializer

expect open class CasuariusStorage {
    fun remove(key: String)

    fun hasValue(key: String): Boolean

    fun clear()

    fun put(key: String, value: String)
    fun string(key: String, defaultValue: String? = null): String?

    fun put(key: String, value: Int)
    fun int(key: String, defaultValue: Int? = null): Int?

    fun put(key: String, value: Float)
    fun float(key: String, defaultValue: Float? = null): Float?

    fun put(key: String, value: Long)
    fun long(key: String, defaultValue: Long?): Long?

    fun put(key: String, value: Double)
    fun double(key: String, defaultValue: Double? = null): Double?

    fun put(key: String, value: Boolean)
    fun boolean(key: String, defaultValue: Boolean? = null): Boolean?

    fun <T> put(key: String, value: T, kSerializer: KSerializer<T>)
    fun <T> serializable(key: String, kSerializer: KSerializer<T>): T?
}