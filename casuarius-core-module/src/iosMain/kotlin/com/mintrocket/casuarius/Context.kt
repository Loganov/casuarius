package com.mintrocket.casuarius

import kotlinx.cinterop.convert
import platform.CoreFoundation.*

class Context(val refs: Map<CFStringRef?, CFTypeRef?>) {
    fun query(vararg pairs: Pair<CFStringRef?, CFTypeRef?>): CFDictionaryRef? {
        val map = mapOf(*pairs).plus(refs.filter { it.value != null })
        return CFDictionaryCreateMutable(
            null, map.size.convert(), null, null
        ).apply {
            map.entries.forEach { CFDictionaryAddValue(this, it.key, it.value) }
        }.apply {
            CFAutorelease(this)
        }
    }
}