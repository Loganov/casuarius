package com.mintrocket.casuarius

import platform.Foundation.*
import platform.darwin.OSStatus
import platform.darwin.noErr

fun String.toNSData(): NSData? =
    NSString.create(string = this).dataUsingEncoding(NSUTF8StringEncoding)

fun NSNumber.toNSData() = NSKeyedArchiver.archivedDataWithRootObject(this)
fun NSData.toNSNumber() = NSKeyedUnarchiver.unarchiveObjectWithData(this) as? NSNumber

fun OSStatus.validate(): Boolean = toUInt() == noErr