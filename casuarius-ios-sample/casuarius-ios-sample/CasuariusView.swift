//
//  CasuariusView.swift
//  casuarius-ios-sample
//
//  Created by Александр Логанов on 07.03.2022.
//

import SwiftUI
import casuarius_shared_module

struct CasuariusView: View {
    
    let storage = CasuariusStorage(serviceName: "Test_Storage").storage
    @State private var textS: NSString = ""
    
    
    var body: some View {
        VStack {
            Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
            Button("Save data", action: saveData).padding()
            Text("Get Data \(textS)")
            Button("Get data", action: getData).padding()
            
        }
    }
    
    func saveData() {
        storage.putString(key: "test_key", value: "Test data")
    }
    
    func getData() {
        let data = storage.getString(key: "test_key", defaultValue: "defaultValue") ?? "defaultValue"
        textS = NSString.init(string: data)
    }
}

struct CasuariusView_Previews: PreviewProvider {
    static var previews: some View {
        CasuariusView()
    }
}
