//
//  casuarius_ios_sampleApp.swift
//  casuarius-ios-sample
//
//  Created by Александр Логанов on 07.03.2022.
//

import SwiftUI

@main
struct casuarius_ios_sampleApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            CasuariusView()
        }
    }
}
