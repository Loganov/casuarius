package com.mintrocket.casuarius

import kotlinx.cinterop.*
import kotlinx.serialization.KSerializer
import platform.CoreFoundation.*
import platform.Foundation.*
import platform.Security.*

/**
 * ios имплементация CasuariusStorage
 *
 * @param storageFileName название файла
 */
actual open class CasuariusStorage(
    private val storageFileName: String
) {

    private val NSData.stringValue: String?
        get() = NSString.create(this, NSUTF8StringEncoding) as String?


    actual fun remove(key: String) {
        context(key) { (account) ->
            val query = query(
                kSecClass to kSecClassGenericPassword,
                kSecAttrAccount to account
            )

            SecItemDelete(query)
        }
    }

    actual fun hasValue(key: String): Boolean {
        return context(key) { (account) ->
            val query = query(
                kSecClass to kSecClassGenericPassword,
                kSecAttrAccount to account
            )
            SecItemCopyMatching(query, null).validate()
        }
    }

    actual fun clear() {
        context {
            val query = query(
                kSecClass to kSecClassGenericPassword
            )
            SecItemDelete(query)
        }
    }

    actual fun put(key: String, value: String) {
        addOrUpdate(key, value.toNSData())
    }

    actual fun string(key: String, defaultValue: String?): String? {
        return value(key)?.stringValue ?: defaultValue
    }

    actual fun put(key: String, value: Int) {
        addOrUpdate(key, NSNumber(int = value).toNSData())
    }

    actual fun int(key: String, defaultValue: Int?): Int? {
        return value(key)?.toNSNumber()?.intValue ?: defaultValue
    }

    actual fun put(key: String, value: Float) {
        addOrUpdate(key, NSNumber(float = value).toNSData())
    }

    actual fun float(key: String, defaultValue: Float?): Float? {
        return value(key)?.toNSNumber()?.floatValue ?: defaultValue
    }

    actual fun put(key: String, value: Long) {
        addOrUpdate(key, NSNumber(long = value).toNSData())
    }

    actual fun long(key: String, defaultValue: Long?): Long? {
        return value(key)?.toNSNumber()?.longValue ?: defaultValue
    }

    actual fun put(key: String, value: Double) {
        addOrUpdate(key, NSNumber(double = value).toNSData())
    }

    actual fun double(key: String, defaultValue: Double?): Double? {
        return value(key)?.toNSNumber()?.doubleValue ?: defaultValue
    }

    actual fun put(key: String, value: Boolean) {
        addOrUpdate(key, NSNumber(bool = value).toNSData())
    }

    actual fun boolean(key: String, defaultValue: Boolean?): Boolean? {
        return value(key)?.toNSNumber()?.boolValue ?: defaultValue
    }

    actual fun <T> put(key: String, value: T, kSerializer: KSerializer<T>) {

    }

    actual fun <T> serializable(key: String, kSerializer: KSerializer<T>): T? {
        return null
    }


    // Helper
    private fun <T> context(vararg values: Any?, block: Context.(List<CFTypeRef?>) -> T): T {
        val standard = mapOf(
            kSecAttrService to CFBridgingRetain(storageFileName),
            kSecAttrAccessGroup to CFBridgingRetain(null)
        )
        val custom = arrayOf(*values).map { CFBridgingRetain(it) }
        return block.invoke(Context(standard), custom).apply {
            standard.values.plus(custom).forEach { CFBridgingRelease(it) }
        }
    }

    private fun add(key: String, value: NSData?) = context(key, value) { (account, data) ->
        val query = query(
            kSecClass to kSecClassGenericPassword,
            kSecAttrAccount to account,
            kSecValueData to data
        )
        SecItemAdd(query, null)
    }

    private fun update(key: String, value: Any?) = context(key, value) { (account, data) ->
        val query = query(
            kSecClass to kSecClassGenericPassword,
            kSecAttrAccount to account,
            kSecReturnData to kCFBooleanFalse
        )

        val updateQuery = query(
            kSecValueData to data
        )

        SecItemUpdate(query, updateQuery)
    }

    private fun addOrUpdate(key: String, value: NSData?) {
        if (hasValue(key)) {
            update(key, value)
        } else {
            add(key, value)
        }
    }

    private fun value(forKey: String): NSData? = context(forKey) { (account) ->
        val query = query(
            kSecClass to kSecClassGenericPassword,
            kSecAttrAccount to account,
            kSecReturnData to kCFBooleanTrue,
            kSecMatchLimit to kSecMatchLimitOne
        )

        memScoped {
            val result = alloc<CFTypeRefVar>()
            SecItemCopyMatching(query, result.ptr)
            CFBridgingRelease(result.value) as? NSData
        }
    }
}